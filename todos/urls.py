from django.urls import path

from todos.views import (
    TodoListView,
    TodoDetailView,
    TodoCreateView,
    TodoUpdateView,
    TodoDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView,
)


urlpatterns = [
    path(
        "items/<int:pk>/edit/",
        TodoItemUpdateView.as_view(),
        name="todo_item_update",
    ),
    path("<int:pk>/delete/", TodoDeleteView.as_view(), name="todo_list_delete"),
    path("create/", TodoCreateView.as_view(), name="todo_list_create"),
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
    path("<int:pk>/edit/", TodoUpdateView.as_view(), name="todo_list_update"),
    path(
        "items/create/", TodoItemCreateView.as_view(), name="todo_item_create"
    ),
]
